%{
Hanon is great for pianists. Now it's great for double bassists!
%}

\header{
	title = "Hanon for Bass"
}


\score
\relative {
	\clef bass
	\key c \major
	\time 2/4
	\repeat volta 2 {
	c16^"Beginner" e f g a g f e | d f g a b a g f | e, g a b c b a g | f a b c d c b a | g b c d e d c b | 
	a c d e f e d c | b d e f g f e d | c e f g a g f e | d f g a b a g f | e g a b c b a g | 
	f a b c d c b a | g b c d e d c b | a c d e f e d c | b d e f g f e d || g e d c b c d e | 
	f d c b a b c d | e c b a g a b c | d b a g f g a b | c a g f e f g a | b g f e d e f g | 
	a f e d c d e f | g e d c b c d e | f d c b a b c d | e c d a g a b c | d b a g f g a b | 
	c a g f e f g a | b' g f e d e f g | a f e d c d e f | g e d c b c d e | } c2  \bar "|."
}
}

\score {
\relative {
	\clef bass
	\key c \major
	\time 2/4
	\repeat volta 2 {
	c16^"Advanced" e f g a g f e | d f g a b a g f | e g a b c b a g | f a b c d c b a | g b c d e d c b | 
	a c d e f e d c | b d e f g f e d | \clef tenor c e f g a g f e | d f g a b a g f | e g a b c b a g | 
	f a b c d c b a | \clef treble g b c d e d c b | a c d e f e d c | b d e f g f e d || g e d c b c d e | 
	f d c b a b c d | e c b a g a b c | \clef tenor d b a g f g a b | c a g f e f g a | b g f e d e f g | 
	a f e d c d e f | g e d c b c d e | f d c b a b c d | \clef bass e c d a g a b c | d b a g f g a b | 
	c a g f e f g a | b g f e d e f g | a f e d c d e f | g e d c b c d e | } c2 \bar "|."
}
}


\version "2.18.2"  % necessary for upgrading to future LilyPond versions.
